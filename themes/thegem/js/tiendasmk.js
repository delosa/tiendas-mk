(function($){

	if($('#user-role').val() != 'um_distribuidor'){
		$('#venta-flash').remove();
		$('.cat-item-422').remove();
		$('.cat-item-359').remove();
		//$('a[data-tab="prize"]').remove();
		//$('a[data-tab="affiliates"]').remove();
	}

    $(function() {
        // Handler for .ready() called.
        $('.woocommerce-product-details__short-description').append('<a data-fancybox data-src="#product-detail" href="javascript:;"><i class="fa fa-info-circle" aria-hidden="true"></i> Conoce más</a>');
    	
        //var topePuntos = $('.top-actual-points').val();
        var puntosActuales = $('.gamipress-user-points-amount').text();
        $('.user-actual-points').val(puntosActuales);
    	$('.user-actual-points').knob({
    		'min': 0,
    		'max': 110,
    		'width': 250,
    		'height': 250,
    		'readOnly': true,
    		'displayInput': true,
    		'fgColor': '#d96f0f',
    		'lineCap': 'round',
    		'thickness':'.15'
    	});

		if($('#logros-actuales-usuario')){
			
			var logro_bronce = false, logro_plata = false, logro_oro = false, logro_ruby = false, logro_platino = false, logro_diamante = false, logro_master= false;
			
			if($('#gamipress-achievement-32832').length > 0){
				logro_bronce = true;
			}

			if($('#gamipress-achievement-32876').length > 0){
				logro_plata = true;
			}

			if($('#gamipress-achievement-32877').length > 0){
				logro_oro = true;
			}

			if($('#gamipress-achievement-32996').length > 0){
				logro_ruby = true;
			}

			if($('#gamipress-achievement-32997').length > 0){
				logro_platino = true;
			}

			if($('#gamipress-achievement-32998').length > 0){
				logro_diamante = true;
			}

			if($('#gamipress-achievement-32999').length > 0){
				logro_master = true;
			}											

			//console.log(logro_bronce, logro_plata, logro_oro);
			obtener_cupones(logro_bronce, logro_plata, logro_oro, logro_ruby, logro_platino, logro_diamante, logro_master);
		}


	});
	
	function obtener_cupones(bronce, plata, oro, ruby, platino, diamante, master){

		if(bronce && plata && oro && ruby && platino && diamante && master){
			$('#bronce-section').find('#premio-bronce-cupon').find('#cupon-bronce').find('i').html($('#bronce-cupon').val());
			$('#plata-section').find('#premio-plata-cupon').find('#cupon-plata').find('i').html($('#plata-cupon').val());
			$('#oro-section').find('#premio-oro-cupon').find('#cupon-oro').find('i').html($('#oro-cupon').val());
			$('#ruby-section').find('#premio-ruby-cupon').find('#cupon-ruby').find('i').html($('#ruby-cupon').val());
			$('#platino-section').find('#premio-platino-cupon').find('#cupon-platino').find('i').html($('#platino-cupon').val());
			$('#diamante-section').find('#premio-diamante-cupon').find('#cupon-diamante').find('i').html($('#diamante-cupon').val());
			$('#master-section').find('#premio-master-cupon').find('#cupon-master').find('i').html($('#master-cupon').val());

			$('#bronce-section').find('#premio-bronce-cupon').find('#cupon-bronce').addClass('cupon-desbloqueado');
			$('#plata-section').find('#premio-plata-cupon').find('#cupon-plata').addClass('cupon-desbloqueado');
			$('#oro-section').find('#premio-oro-cupon').find('#cupon-oro').addClass('cupon-desbloqueado');
			$('#ruby-section').find('#premio-ruby-cupon').find('#cupon-ruby').addClass('cupon-desbloqueado');
			$('#platino-section').find('#premio-platino-cupon').find('#cupon-platino').addClass('cupon-desbloqueado');
			$('#diamante-section').find('#premio-diamante-cupon').find('#cupon-diamante').addClass('cupon-desbloqueado');
			$('#master-section').find('#premio-master-cupon').find('#cupon-master').addClass('cupon-desbloqueado');

		}

		if(bronce && plata && oro && ruby && platino && diamante && !master){
			$('#bronce-section').find('#premio-bronce-cupon').find('#cupon-bronce').find('i').html($('#bronce-cupon').val());
			$('#plata-section').find('#premio-plata-cupon').find('#cupon-plata').find('i').html($('#plata-cupon').val());
			$('#oro-section').find('#premio-oro-cupon').find('#cupon-oro').find('i').html($('#oro-cupon').val());
			$('#ruby-section').find('#premio-ruby-cupon').find('#cupon-ruby').find('i').html($('#ruby-cupon').val());
			$('#platino-section').find('#premio-platino-cupon').find('#cupon-platino').find('i').html($('#platino-cupon').val());
			$('#diamante-section').find('#premio-diamante-cupon').find('#cupon-diamante').find('i').html($('#diamante-cupon').val());

			$('#bronce-section').find('#premio-bronce-cupon').find('#cupon-bronce').addClass('cupon-desbloqueado');
			$('#plata-section').find('#premio-plata-cupon').find('#cupon-plata').addClass('cupon-desbloqueado');
			$('#oro-section').find('#premio-oro-cupon').find('#cupon-oro').addClass('cupon-desbloqueado');
			$('#ruby-section').find('#premio-ruby-cupon').find('#cupon-ruby').addClass('cupon-desbloqueado');
			$('#platino-section').find('#premio-platino-cupon').find('#cupon-platino').addClass('cupon-desbloqueado');
			$('#diamante-section').find('#premio-diamante-cupon').find('#cupon-diamante').addClass('cupon-desbloqueado');

			$('#master-cupon').remove();

		}

		if(bronce && plata && oro && ruby && platino && !diamante && !master){
			$('#bronce-section').find('#premio-bronce-cupon').find('#cupon-bronce').find('i').html($('#bronce-cupon').val());
			$('#plata-section').find('#premio-plata-cupon').find('#cupon-plata').find('i').html($('#plata-cupon').val());
			$('#oro-section').find('#premio-oro-cupon').find('#cupon-oro').find('i').html($('#oro-cupon').val());
			$('#ruby-section').find('#premio-ruby-cupon').find('#cupon-ruby').find('i').html($('#ruby-cupon').val());
			$('#platino-section').find('#premio-platino-cupon').find('#cupon-platino').find('i').html($('#platino-cupon').val());

			$('#bronce-section').find('#premio-bronce-cupon').find('#cupon-bronce').addClass('cupon-desbloqueado');
			$('#plata-section').find('#premio-plata-cupon').find('#cupon-plata').addClass('cupon-desbloqueado');
			$('#oro-section').find('#premio-oro-cupon').find('#cupon-oro').addClass('cupon-desbloqueado');
			$('#ruby-section').find('#premio-ruby-cupon').find('#cupon-ruby').addClass('cupon-desbloqueado');
			$('#platino-section').find('#premio-platino-cupon').find('#cupon-platino').addClass('cupon-desbloqueado');

			$('#diamante-cupon').remove();
			$('#master-cupon').remove();

		}
		
		if(bronce && plata && oro && ruby && !platino && !diamante && !master){
			$('#bronce-section').find('#premio-bronce-cupon').find('#cupon-bronce').find('i').html($('#bronce-cupon').val());
			$('#plata-section').find('#premio-plata-cupon').find('#cupon-plata').find('i').html($('#plata-cupon').val());
			$('#oro-section').find('#premio-oro-cupon').find('#cupon-oro').find('i').html($('#oro-cupon').val());
			$('#ruby-section').find('#premio-ruby-cupon').find('#cupon-ruby').find('i').html($('#ruby-cupon').val());

			$('#bronce-section').find('#premio-bronce-cupon').find('#cupon-bronce').addClass('cupon-desbloqueado');
			$('#plata-section').find('#premio-plata-cupon').find('#cupon-plata').addClass('cupon-desbloqueado');
			$('#oro-section').find('#premio-oro-cupon').find('#cupon-oro').addClass('cupon-desbloqueado');
			$('#ruby-section').find('#premio-ruby-cupon').find('#cupon-ruby').addClass('cupon-desbloqueado');

			$('#platino-cupon').remove();
			$('#diamante-cupon').remove();
			$('#master-cupon').remove();

		}

		if(bronce && plata && oro && !ruby && !platino && !diamante && !master){
			$('#bronce-section').find('#premio-bronce-cupon').find('#cupon-bronce').find('i').html($('#bronce-cupon').val());
			$('#plata-section').find('#premio-plata-cupon').find('#cupon-plata').find('i').html($('#plata-cupon').val());
			$('#oro-section').find('#premio-oro-cupon').find('#cupon-oro').find('i').html($('#oro-cupon').val());

			$('#bronce-section').find('#premio-bronce-cupon').find('#cupon-bronce').addClass('cupon-desbloqueado');
			$('#plata-section').find('#premio-plata-cupon').find('#cupon-plata').addClass('cupon-desbloqueado');
			$('#oro-section').find('#premio-oro-cupon').find('#cupon-oro').addClass('cupon-desbloqueado');

			$('#ruby-cupon').remove();
			$('#platino-cupon').remove();
			$('#diamante-cupon').remove();
			$('#master-cupon').remove();

		}

		if(bronce && plata && !oro && !ruby && !platino && !diamante && !master){
			$('#bronce-section').find('#premio-bronce-cupon').find('#cupon-bronce').find('i').html($('#bronce-cupon').val());
			$('#plata-section').find('#premio-plata-cupon').find('#cupon-plata').find('i').html($('#plata-cupon').val());

			$('#bronce-section').find('#premio-bronce-cupon').find('#cupon-bronce').addClass('cupon-desbloqueado');
			$('#plata-section').find('#premio-plata-cupon').find('#cupon-plata').addClass('cupon-desbloqueado');

			$('#oro-cupon').remove();
			$('#ruby-cupon').remove();
			$('#platino-cupon').remove();
			$('#diamante-cupon').remove();
			$('#master-cupon').remove();
		}

		if(bronce && !plata && !oro && !ruby && !platino && !diamante && !master){
			console.log('hello');
			$('#bronce-section').find('#premio-bronce-cupon').find('#cupon-bronce').find('i').html($('#bronce-cupon').val());

			$('#bronce-section').find('#premio-bronce-cupon').find('#cupon-bronce').addClass('cupon-desbloqueado');

			$('#plata-cupon').remove();
			$('#oro-cupon').remove();
			$('#ruby-cupon').remove();
			$('#platino-cupon').remove();
			$('#diamante-cupon').remove();
			$('#master-cupon').remove();
		}
	}

		

})(jQuery);
