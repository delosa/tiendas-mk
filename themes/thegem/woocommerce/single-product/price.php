<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>
<p class="price-label">Precio:</p>
<?php if (empty($product->sale_price)) :?>
	<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) );?>">
		<ins>
			<span class="woocommerce-Price-amount amount">
				<span class="woocommerce-Price-currencySymbol">&#36;</span>
				<?php echo number_format($product->price, 0, ",", "."); ?>
			</span>
		</ins>
	</p>
<?php endif; ?>
<?php if (!empty($product->sale_price)) :?>
	<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) );?>">
		<ins>
			<span class="woocommerce-Price-amount amount">
				<span class="woocommerce-Price-currencySymbol">&#36;</span>
				<?php echo number_format($product->sale_price, 0, ",", "."); ?>
			</span>
		</ins>
	</p>
	<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) );?> discount-price">
		<del>
			<span class="woocommerce-Price-amount amount">
				<span class="woocommerce-Price-currencySymbol">&#36;</span>
				<?php echo number_format($product->regular_price, 0, ",", "."); ?>
			</span>
		</del>
	</p>
<?php endif; ?>
<!-- <?php print_r($product); ?> -->
<?php //echo $product->get_price_html(); ?>
