<?php
global $wpdb;
$affiliate_id = affwp_get_affiliate_id();
$user = affwp_get_affiliate_username();
//ini_set('display_errors', 'On');
//error_reporting(E_ALL);
?>
<div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>
<div class="urlcontainer">
	<p class="referido"><a href="http://tiendasmk.com/producto/kit-distribuidor/ref/<?php echo affwp_get_affiliate_username() ?>/">http://tiendasmk.com/producto/kit-distribuidor/ref/<?php echo affwp_get_affiliate_username() ?>/</a></p>
</div>


<div id="affwp-affiliate-dashboard-referrals" class="affwp-tab-content">

	<h4>Distribuidores Referidos</h4>

	<?php
	$per_page  = 50;
	$page      = affwp_get_current_page_number();
	$pages     = absint( ceil( affwp_count_referrals( $affiliate_id ) / $per_page ) );
	$referrals = affiliate_wp()->referrals->get_referrals(
		array(
			'number'       => $per_page,
			'offset'       => $per_page * ( $page - 1 ),
			'affiliate_id' => $affiliate_id,
			'status'       => array( 'paid', 'unpaid', 'rejected' ),
		)
	);
	?>

	<?php
	/**
	 * Fires before the referrals dashbaord data able within the referrals template.
	 *
	 * @param int $affiliate_id Affiliate ID.
	 */
	do_action( 'affwp_referrals_dashboard_before_table', $affiliate_id );
	
	$datos = $wpdb->get_results('SELECT * FROM `wp_cupon_referral` WHERE `cup_affiliate_id` = '.$affiliate_id.' ORDER BY `cup_referral_id` DESC;');
	$num_cupones = 0;
	?>
	<table id="affwp-affiliate-dashboard-referrals" class="affwp-table affwp-table-responsive">
		<thead>
			<tr>
				<th class="referral-amount"><?php _e( 'Cantidad de Puntos', 'affiliate-wp' ); ?></th>
				<th class="referral-nombre"><?php _e( 'Nombre y Apellidos', 'affiliate-wp' ); ?></th>
				<th class="referral-date"><?php _e( 'Fecha de Registro', 'affiliate-wp' ); ?></th>
				<th class="referral-coupon"><?php _e( 'Regalo', 'affiliate-wp' ); ?></th>
				<?php
				/**
				 * Fires in the dashboard referrals template, within the table header element.
				 */
				do_action( 'affwp_referrals_dashboard_th' );
				?>
			</tr>
		</thead>

		<tbody>
			<?php if ( $referrals ) : 
				

				$monto_total = 0;
				?>

				<?php 
				
				$i = 0;
				//Este ciclo es para solo buscar los distribuidores e imprimirlos 
				foreach ( $referrals as $prueba1 ) {
					$description1 = $prueba1->description;
					if($description1 == "Kit distribuidor"){
						$prueba1->amount = 0;
						$distribuidores[$i] = $prueba1;//todos los distribuidores
					}
					$i++;
				}
				
				$log = 1;
				//$k = 0;
				foreach ($distribuidores as $dist1) {
					$dist_cus1 = $dist1->customer_id;
					$dist_desc1 = $dist1->description;
					$amount = $dist1->amount; 
					$puntos = 0;
					
					foreach ($referrals as $prueba2) {
						$cust2 = $prueba2->customer_id;
						$description2 = $prueba2->description;
						$cant = $prueba2->amount;
						$num = $cant / 3000;
						if($cust2 == $dist_cus1 && $description2 != $dist_desc1){
							if($prueba2->reference != 35197){
								$order = wc_get_order( $prueba2->reference );
								/*echo '<pre>';
								var_dump($order);*/
							
								//echo $prueba2->reference . ' = referencia';
								/*echo '<pre>';
								var_dump($order);*/
								$items = $order->get_items();
								$i = 0;
								$puntos += $amount;
							/*	echo 'ITEMS';
								echo '<pre>';
								var_dump($items);*/
								foreach ( $items as $item ) {
									/*echo 'Entre al foreach <br> <br>'; 
									echo '<pre>';
									var_dump($item);*/
									if($i < $log){
										//echo 'Entre al if <br> <br>';
										$puntos += $item->get_quantity();
									}
								}
							}
						}
					}
					$dist1->amount = $puntos;
					/*echo 'DISTRIBUIDORES';
					echo '<pre>';
					var_dump($distribuidores);*/
					//$prueba[$k] = $dist1;
					/*echo 'DISTRIBUIDORES';
					echo '<pre>';
					var_dump($prueba);*/
					//$k++;
				}
			/*	echo 'DISTRIBUIDORES';
				echo '<pre>';
				var_dump($prueba);*/
				$monto = 0;
				
			

				foreach ( $distribuidores as $referral ) : ?>
					
					<tr>
						<?php 
							$order = wc_get_order( $referral->reference );

							$cant = $referral->amount;	
							
							//Para saber si un cupon ya esta disponible o no 
							$cod_cupon = $datos[$num_cupones]->cup_cupon;
							$visit = $datos[$num_cupones]->cup_visitado;
							$id_cupon = $datos[$num_cupones]->cup_id;
						?>
						<td class="referral-amount" data-th="<?php _e( 'Amount', 'affiliate-wp' ); ?>"><?php echo $cant;?></td>
						<?php
							$monto += $cant; 
						?>
						<td class="referral-nombre" data-th="<?php _e( 'Description', 'affiliate-wp' ); ?>"><?php echo $order->get_billing_first_name() . " " . $order->get_billing_last_name(); /*wp_kses_post( nl2br( $referral->description ) );*/ ?></td>
						<td class="referral-date" data-th="<?php _e( 'Date', 'affiliate-wp' ); ?>"><?php echo esc_html( $referral->date_i18n( 'datetime' ) ); ?></td>
						<td class="referral-coupon" data-th="<?php _e( 'Regalo', 'affiliate-wp' ); ?>">
								<?php if($visit == 1): ?>
									<p style="text-align: center;">Genera tu cupón</p>
									<input class="id_cupon" type="hidden" name="id_cupon<?php echo $id_cupon; ?>" value="<?php echo $id_cupon; ?>">
									<input class="visitado" type="hidden" name="visit<?php echo $id_cupon; ?>" value="<?php echo $visit; ?>">
									<input class="cupon" type="hidden" name="cupon<?php echo $id_cupon; ?>" value="<?php echo $cod_cupon; ?>">
									<a class="gen_cupon" href='#' style="text-align:center;">Generar</a>
								<?php else: ?>
									<p>Ya utilizaste tu regalo. Sigue refiriendo</p>
								<?php endif;?>
						</td>
						<?php 
						/**
						 * Fires within the table data of the dashboard referrals template.
						 *
						 * @param \AffWP\Referral $referral Referral object.
						 */
						do_action( 'affwp_referrals_dashboard_td', $referral ); ?>
						
					</tr>
					
				<?php 
				$num_cupones += 1;
				endforeach;?>
			<?php else : ?>

				<tr>
					<td class="affwp-table-no-data" colspan="5"><?php _e( 'No tienes referidos aun, pero los puedes tener ya!.', 'affiliate-wp' ); ?></td>
				</tr>

			<?php endif; ?>
		</tbody>
	</table>
	<?php
	
	$consulta = $wpdb->get_row('SELECT * FROM `wp_total_points` WHERE `tot_affiliate_id` = '.$affiliate_id.' ORDER BY `tot_id` DESC;');
	if($monto > $consulta->tot_puntos_dist){
		$descuento = $consulta->tot_puntos_usados / 2;
		$monto -= $consulta->tot_puntos_dist;
		$monto -= $descuento;
		$monto += $consulta->tot_puntos_dist;
	}

	?>

	<p>Cantidad Total Disponible: <?php echo $monto; ?><p>
	<?php $cupon_desc = do_shortcode( '[shareyourfriend]' ); ?>
	<h4>Cupón de Regalo para familiares y amigos</h4>
	<p>* Cupón que equivale al 10% de descuento en cada compra</p>
	<p>* Solo puede ser utilizado por clientes que no son Distribuidores<p>
	<p>Cupón: <strong><?php echo $cupon_desc;?></strong></p>
	<div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>
	
	<table>
		<thead>
			<tr>
				<th class="cupon-puntos"><?php _e( 'Cantidad de Puntos', 'affiliate-wp' ); ?></th>
				<th class="cupon-nombre-referido"><?php _e( 'Nombre del Usuario', 'affiliate-wp' ); ?></th>
				<th class="cupon-orden-id"><?php _e( '# Orden', 'affiliate-wp' ); ?></th>
				<?php
				/**
				 * Fires in the dashboard referrals template, within the table header element.
				 */
				do_action( 'affwp_referrals_dashboard_th' );
				?>
			</tr>
		</thead>
		<tbody>
			<?php if ( $affiliate_id ) : 
			
				//datos order cupon, trae todos los cupones que se usaron en las compras
				//con todos sus datos por filas
				$datos_order_cupon = $wpdb->get_results('SELECT * FROM `wp_woocommerce_order_items` WHERE `order_item_name` = "'.$user.'" ORDER BY `order_item_id` DESC;');

				$ind = 0;
				//datos order cupon contiene toda la tabla woocommerce order items
				//este ciclo es para sacar los pedidos donde se usaron el cupon del afiliado 
				$j=0;
				foreach ($datos_order_cupon as $cupones_porcent){
					$num_order = $cupones_porcent->order_id;

					$datos_order_pedidos = $wpdb->get_results('SELECT * FROM `wp_woocommerce_order_items` WHERE `order_id` = '.$num_order.' ORDER BY `order_item_id` DESC;');
					
					foreach ($datos_order_pedidos as $pedidos_same) {
						
						if($pedidos_same->order_item_type == "line_item" && $pedidos_same->order_item_name != "Kit distribuidor"){
							
							$pedidos_same->order_item_id = 0;
							$pedidos[$ind] = $pedidos_same;
							$ind++;
						}
						
					}			
					$log = count($pedidos);

					foreach ($pedidos as $ped){	
						$order = wc_get_order($ped->order_id);
						$items = $order->get_items();
						$i = 0;
						$puntos = 0;
						foreach ( $items as $item ) {
							if($i < $log)
							{
								$puntos += $item->get_quantity();
							}
						}
						$ped->order_item_id = $puntos;
						$final[$j] = $ped;
						$pedidos = NULL;
					}
					$j++;
				}
			
				$num_cupones = 0;

				foreach ( $final as $ped ) : ?>
					<?php
						$puntos = $ped->order_item_id;
						//Obtiene la orden del woocommerce con el ID
						$order = wc_get_order($ped->order_id);							
						
						$items = $order->get_items();
						
						$referral_first_name = $order->billing_first_name;	
						$referral_last_name = $order->billing_last_name;
					?>
					<tr>
						<td class="cupon-puntos" data-th="<?php _e( 'Cantidad de Puntos', 'affiliate-wp' ); ?>"><?php echo $puntos;?></td>
						<td class="referral-nombre"><?php echo $referral_first_name . ' ' . $referral_last_name ?></td>
						<td class="cupon-orden-id" data-th="<?php _e( '# Orden', 'affiliate-wp' ); ?>"><?php echo $ped->order_id;?></td>
						<?php $puntos_sum += $puntos; ?>
					</tr>
				
				<?php endforeach;?>
			<?php else : ?>
				<tr>
					<td class="affwp-table-no-data" colspan="5"><?php _e( 'No tienes referidos aun, pero los puedes tener ya!.', 'affiliate-wp' ); ?></td>
				</tr>
			<?php endif; ?>
		</tbody>



	</table>
	<div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>

	<?php
	$consulta = $wpdb->get_row('SELECT * FROM `wp_total_points` WHERE `tot_affiliate_id` = '.$affiliate_id.' ORDER BY `tot_id` DESC;');
	
	if($puntos_sum > $consulta->tot_puntos_cup){
		$descuento = $consulta->tot_puntos_usados / 2;
		$puntos_sum -= $consulta->tot_puntos_cup;
		$puntos_sum -= $descuento;
		$puntos_sum += $consulta->tot_puntos_cup;
	}

	?>
	<p>Cantidad Total Disponible: <?php echo $puntos_sum; 
	$monto_total = $puntos_sum + $monto;
	?><p>
	<div class="vc_empty_space" style="height: 40px"><span class="vc_empty_space_inner"></span></div>
	<h4>TOTAL DE PUNTOS: <?php echo $monto_total; ?></h4>


	<div class="vc_empty_space" style="height: 40px"><span class="vc_empty_space_inner"></span></div>
	
	<?php
		$fecha = date("y-m-d:H");
		

		$consulta = $wpdb->get_row('SELECT * FROM `wp_total_points` WHERE `tot_affiliate_id` = '.$affiliate_id.' ORDER BY `tot_id` DESC;');
		/*echo '<pre>';
		var_dump($consulta);*/


		if($monto_total > $consulta->tot_total)
		{
			if(isset($consulta->tot_affiliate_id)){
				$insercion_puntos = $wpdb->query('INSERT INTO wp_total_points (tot_affiliate_id, tot_puntos_dist, tot_puntos_cup, tot_total, tot_fecha)
				VALUES ("'.$affiliate_id.'", "'.$monto.'", "'.$puntos_sum.'", "'.$monto_total.'",  "'.$fecha.'")');
			}
	
			if(!$insercion_puntos) {
				return false;
			}
		}

		$usados = $consulta->tot_puntos_usados;
		
	?>

	<?php //Cupones
	$cupon_10_descuento = do_shortcode( '[premioReferidos10]' );
	$cupon_20_descuento = do_shortcode( '[premioReferidos20]' );
	$cupon_30_descuento = do_shortcode( '[premioReferidos30]' );
	$cupon_40_descuento = do_shortcode( '[premioReferidos40]' );
	$cupon_50_descuento = do_shortcode( '[premioReferidos50]' );

	$cupon10 = 10;
	$cupon20 = 20;
	$cupon30 = 30;
	$cupon40 = 40;
	$cupon50 = 50;
	
	?>

	<h2>Redimir Puntos</h2>
	<div class="vc_empty_space" style="height: 10px"><span class="vc_empty_space_inner"></span></div>
	<div class="vc_row wpb_row vc_inner vc_row-fluid">
	<?php if($monto_total < 10): ?>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<a class="btn btn-inactive" href="#">10 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<a class="btn btn-inactive" href="#">20 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<a class="btn btn-inactive" href="#">30 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<a class="btn btn-inactive" href="#">40 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<a class="btn btn-inactive" href="#">50 puntos</a>
			</div>
		</div>
	<?php else: if($monto_total > 9 && $monto_total < 20):?>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<!-- <a class="btn btn-primary" href="https://www.tiendasmk.com/cupones-por-10-puntos-referidos/">10 puntos</a> -->
				<!-- Id del Afiliado -->
				<!-- Id del Afiliado -->
				<!-- Puntos a Utilizar -->
				<!-- Puntos Distribuidores Referidos -->
				<!-- Puntos Cupones Descuento -->
				<!-- Cupon por Boton-->
				<input class="id_affiliate" type="hidden" name="id_affiliate<?php echo $affiliate_id; ?>" value="<?php echo $affiliate_id; ?>">
				<input class="punt_use" type="hidden" name="punt_use<?php echo $cupon10; ?>" value="<?php echo $cupon10; ?>">
				<input class="punt_dist" type="hidden" name="punt_dist<?php echo $monto; ?>" value="<?php echo $monto; ?>">
				<input class="punt_cupDesc" type="hidden" name="punt_cupDesc<?php echo $puntos_sum; ?>" value="<?php echo $puntos_sum; ?>">
				<input class="punt_usados" type="hidden" name="punt_usados<?php echo $usados; ?>" value="<?php echo $usados; ?>">
				<input class="cupon" type="hidden" name="cupon<?php echo $cupon_10_descuento; ?>" value="<?php echo $cupon_10_descuento; ?>">
				<a class="punt_cupon btn btn-primary" href="#">10 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<a class="btn btn-inactive" href="">20 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<a class="btn btn-inactive" href="#">30 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<a class="btn btn-inactive" href="#">40 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<a class="btn btn-inactive" href="#">50 puntos</a>
			</div>
		</div>
		<?php endif; 
		if($monto_total > 19 && $monto_total < 30):?>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<!-- Id del Afiliado -->
				<!-- Puntos a Utilizar -->
				<!-- Puntos Distribuidores Referidos -->
				<!-- Puntos Cupones Descuento -->
				<!-- Cupon por Boton-->
				<input class="id_affiliate" type="hidden" name="id_affiliate<?php echo $affiliate_id; ?>" value="<?php echo $affiliate_id; ?>">
				<input class="punt_use" type="hidden" name="punt_use<?php echo $cupon10; ?>" value="<?php echo $cupon10; ?>">
				<input class="punt_dist" type="hidden" name="punt_dist<?php echo $monto; ?>" value="<?php echo $monto; ?>">
				<input class="punt_cupDesc" type="hidden" name="punt_cupDesc<?php echo $puntos_sum; ?>" value="<?php echo $puntos_sum; ?>">
				<input class="punt_usados" type="hidden" name="punt_usados<?php echo $usados; ?>" value="<?php echo $usados; ?>">
				<input class="cupon" type="hidden" name="cupon<?php echo $cupon_10_descuento; ?>" value="<?php echo $cupon_10_descuento; ?>">
				<a class="punt_cupon btn btn-primary" href="#">10 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<!-- <a class="btn btn-primary" href="https://www.tiendasmk.com/cupones-por-10-puntos-referidos/">10 puntos</a> -->
				<!-- Id del Afiliado -->
				<!-- Id del Afiliado -->
				<!-- Puntos a Utilizar -->
				<!-- Puntos Distribuidores Referidos -->
				<!-- Puntos Cupones Descuento -->
				<!-- Cupon por Boton-->
				<input class="id_affiliate" type="hidden" name="id_affiliate<?php echo $affiliate_id; ?>" value="<?php echo $affiliate_id; ?>">
				<input class="punt_use" type="hidden" name="punt_use<?php echo $cupon20; ?>" value="<?php echo $cupon20; ?>">
				<input class="punt_dist" type="hidden" name="punt_dist<?php echo $monto; ?>" value="<?php echo $monto; ?>">
				<input class="punt_cupDesc" type="hidden" name="punt_cupDesc<?php echo $puntos_sum; ?>" value="<?php echo $puntos_sum; ?>">
				<input class="punt_usados" type="hidden" name="punt_usados<?php echo $usados; ?>" value="<?php echo $usados; ?>">
				<input class="cupon" type="hidden" name="cupon<?php echo $cupon_20_descuento; ?>" value="<?php echo $cupon_20_descuento; ?>">
				<a class="punt_cupon btn btn-primary" href="#">20 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<a class="btn btn-inactive" href="#">30 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<a class="btn btn-inactive" href="#">40 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<a class="btn btn-inactive" href="#">50 puntos</a>
			</div>
		</div>
		<?php endif; 
		if($monto_total > 29 && $monto_total < 40):?>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<!-- <a class="btn btn-primary" href="https://www.tiendasmk.com/cupones-por-10-puntos-referidos/">10 puntos</a> -->
				<!-- Id del Afiliado -->
				<!-- Puntos a Utilizar -->
				<!-- Puntos Distribuidores Referidos -->
				<!-- Puntos Cupones Descuento -->
				<!-- Cupon por Boton-->
				<input class="id_affiliate" type="hidden" name="id_affiliate<?php echo $affiliate_id; ?>" value="<?php echo $affiliate_id; ?>">
				<input class="punt_use" type="hidden" name="punt_use<?php echo $cupon10; ?>" value="<?php echo $cupon10; ?>">
				<input class="punt_dist" type="hidden" name="punt_dist<?php echo $monto; ?>" value="<?php echo $monto; ?>">
				<input class="punt_cupDesc" type="hidden" name="punt_cupDesc<?php echo $puntos_sum; ?>" value="<?php echo $puntos_sum; ?>">
				<input class="punt_usados" type="hidden" name="punt_usados<?php echo $usados; ?>" value="<?php echo $usados; ?>">
				<input class="cupon" type="hidden" name="cupon<?php echo $cupon_10_descuento; ?>" value="<?php echo $cupon_10_descuento; ?>">
				<a class="punt_cupon btn btn-primary" href="#">10 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<!-- <a class="btn btn-primary" href="https://www.tiendasmk.com/cupones-por-10-puntos-referidos/">10 puntos</a> -->
				<!-- Id del Afiliado -->
				<!-- Id del Afiliado -->
				<!-- Puntos a Utilizar -->
				<!-- Puntos Distribuidores Referidos -->
				<!-- Puntos Cupones Descuento -->
				<!-- Cupon por Boton-->
				<input class="id_affiliate" type="hidden" name="id_affiliate<?php echo $affiliate_id; ?>" value="<?php echo $affiliate_id; ?>">
				<input class="punt_use" type="hidden" name="punt_use<?php echo $cupon20; ?>" value="<?php echo $cupon20; ?>">
				<input class="punt_dist" type="hidden" name="punt_dist<?php echo $monto; ?>" value="<?php echo $monto; ?>">
				<input class="punt_cupDesc" type="hidden" name="punt_cupDesc<?php echo $puntos_sum; ?>" value="<?php echo $puntos_sum; ?>">
				<input class="punt_usados" type="hidden" name="punt_usados<?php echo $usados; ?>" value="<?php echo $usados; ?>">
				<input class="cupon" type="hidden" name="cupon<?php echo $cupon_20_descuento; ?>" value="<?php echo $cupon_20_descuento; ?>">
				<a class="punt_cupon btn btn-primary" href="#">20 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<!-- <a class="btn btn-primary" href="https://www.tiendasmk.com/cupones-por-10-puntos-referidos/">10 puntos</a> -->
				<!-- Id del Afiliado -->
				<!-- Id del Afiliado -->
				<!-- Puntos a Utilizar -->
				<!-- Puntos Distribuidores Referidos -->
				<!-- Puntos Cupones Descuento -->
				<!-- Cupon por Boton-->
				<input class="id_affiliate" type="hidden" name="id_affiliate<?php echo $affiliate_id; ?>" value="<?php echo $affiliate_id; ?>">
				<input class="punt_use" type="hidden" name="punt_use<?php echo $cupon30; ?>" value="<?php echo $cupon30; ?>">
				<input class="punt_dist" type="hidden" name="punt_dist<?php echo $monto; ?>" value="<?php echo $monto; ?>">
				<input class="punt_cupDesc" type="hidden" name="punt_cupDesc<?php echo $puntos_sum; ?>" value="<?php echo $puntos_sum; ?>">
				<input class="punt_usados" type="hidden" name="punt_usados<?php echo $usados; ?>" value="<?php echo $usados; ?>">
				<input class="cupon" type="hidden" name="cupon<?php echo $cupon_30_descuento; ?>" value="<?php echo $cupon_30_descuento; ?>">
				<a class="punt_cupon btn btn-primary" href="#">30 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<a class="btn btn-inactive" href="#">40 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<a class="btn btn-inactive" href="#">50 puntos</a>
			</div>
		</div>
		<?php endif; 
		if($monto_total > 39 && $monto_total < 50):?>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<!-- Id del Afiliado -->
				<!-- Puntos a Utilizar -->
				<!-- Puntos Distribuidores Referidos -->
				<!-- Puntos Cupones Descuento -->
				<!-- Cupon por Boton-->
				<input class="id_affiliate" type="hidden" name="id_affiliate<?php echo $affiliate_id; ?>" value="<?php echo $affiliate_id; ?>">
				<input class="punt_use" type="hidden" name="punt_use<?php echo $cupon10; ?>" value="<?php echo $cupon10; ?>">
				<input class="punt_dist" type="hidden" name="punt_dist<?php echo $monto; ?>" value="<?php echo $monto; ?>">
				<input class="punt_cupDesc" type="hidden" name="punt_cupDesc<?php echo $puntos_sum; ?>" value="<?php echo $puntos_sum; ?>">
				<input class="punt_usados" type="hidden" name="punt_usados<?php echo $usados; ?>" value="<?php echo $usados; ?>">
				<input class="cupon" type="hidden" name="cupon<?php echo $cupon_10_descuento; ?>" value="<?php echo $cupon_10_descuento; ?>">
				<a class="punt_cupon btn btn-primary" href="#">10 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<!-- <a class="btn btn-primary" href="https://www.tiendasmk.com/cupones-por-10-puntos-referidos/">10 puntos</a> -->
				<!-- Id del Afiliado -->
				<!-- Id del Afiliado -->
				<!-- Puntos a Utilizar -->
				<!-- Puntos Distribuidores Referidos -->
				<!-- Puntos Cupones Descuento -->
				<!-- Cupon por Boton-->
				<input class="id_affiliate" type="hidden" name="id_affiliate<?php echo $affiliate_id; ?>" value="<?php echo $affiliate_id; ?>">
				<input class="punt_use" type="hidden" name="punt_use<?php echo $cupon20; ?>" value="<?php echo $cupon20; ?>">
				<input class="punt_dist" type="hidden" name="punt_dist<?php echo $monto; ?>" value="<?php echo $monto; ?>">
				<input class="punt_cupDesc" type="hidden" name="punt_cupDesc<?php echo $puntos_sum; ?>" value="<?php echo $puntos_sum; ?>">
				<input class="punt_usados" type="hidden" name="punt_usados<?php echo $usados; ?>" value="<?php echo $usados; ?>">
				<input class="cupon" type="hidden" name="cupon<?php echo $cupon_20_descuento; ?>" value="<?php echo $cupon_20_descuento; ?>">
				<a class="punt_cupon btn btn-primary" href="#">20 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<!-- <a class="btn btn-primary" href="https://www.tiendasmk.com/cupones-por-10-puntos-referidos/">10 puntos</a> -->
				<!-- Id del Afiliado -->
				<!-- Id del Afiliado -->
				<!-- Puntos a Utilizar -->
				<!-- Puntos Distribuidores Referidos -->
				<!-- Puntos Cupones Descuento -->
				<!-- Cupon por Boton-->
				<input class="id_affiliate" type="hidden" name="id_affiliate<?php echo $affiliate_id; ?>" value="<?php echo $affiliate_id; ?>">
				<input class="punt_use" type="hidden" name="punt_use<?php echo $cupon30; ?>" value="<?php echo $cupon30; ?>">
				<input class="punt_dist" type="hidden" name="punt_dist<?php echo $monto; ?>" value="<?php echo $monto; ?>">
				<input class="punt_cupDesc" type="hidden" name="punt_cupDesc<?php echo $puntos_sum; ?>" value="<?php echo $puntos_sum; ?>">
				<input class="punt_usados" type="hidden" name="punt_usados<?php echo $usados; ?>" value="<?php echo $usados; ?>">
				<input class="cupon" type="hidden" name="cupon<?php echo $cupon_30_descuento; ?>" value="<?php echo $cupon_30_descuento; ?>">
				<a class="punt_cupon btn btn-primary" href="#">30 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<!-- <a class="btn btn-primary" href="https://www.tiendasmk.com/cupones-por-10-puntos-referidos/">10 puntos</a> -->
				<!-- Id del Afiliado -->
				<!-- Id del Afiliado -->
				<!-- Puntos a Utilizar -->
				<!-- Puntos Distribuidores Referidos -->
				<!-- Puntos Cupones Descuento -->
				<!-- Cupon por Boton-->
				<input class="id_affiliate" type="hidden" name="id_affiliate<?php echo $affiliate_id; ?>" value="<?php echo $affiliate_id; ?>">
				<input class="punt_use" type="hidden" name="punt_use<?php echo $cupon40; ?>" value="<?php echo $cupon40; ?>">
				<input class="punt_dist" type="hidden" name="punt_dist<?php echo $monto; ?>" value="<?php echo $monto; ?>">
				<input class="punt_cupDesc" type="hidden" name="punt_cupDesc<?php echo $puntos_sum; ?>" value="<?php echo $puntos_sum; ?>">
				<input class="punt_usados" type="hidden" name="punt_usados<?php echo $usados; ?>" value="<?php echo $usados; ?>">
				<input class="cupon" type="hidden" name="cupon<?php echo $cupon_40_descuento; ?>" value="<?php echo $cupon_40_descuento; ?>">
				<a class="punt_cupon btn btn-primary" href="#">40 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<a class="btn btn-inactive" href="#">50 puntos</a>
			</div>
		</div>
		<?php endif; 
		if($monto_total > 49 ):?>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<!-- Id del Afiliado -->
				<!-- Puntos a Utilizar -->
				<!-- Puntos Distribuidores Referidos -->
				<!-- Puntos Cupones Descuento -->
				<!-- Cupon por Boton-->
				<input class="id_affiliate" type="hidden" name="id_affiliate<?php echo $affiliate_id; ?>" value="<?php echo $affiliate_id; ?>">
				<input class="punt_use" type="hidden" name="punt_use<?php echo $cupon10; ?>" value="<?php echo $cupon10; ?>">
				<input class="punt_dist" type="hidden" name="punt_dist<?php echo $monto; ?>" value="<?php echo $monto; ?>">
				<input class="punt_cupDesc" type="hidden" name="punt_cupDesc<?php echo $puntos_sum; ?>" value="<?php echo $puntos_sum; ?>">
				<input class="punt_usados" type="hidden" name="punt_usados<?php echo $usados; ?>" value="<?php echo $usados; ?>">
				<input class="cupon" type="hidden" name="cupon<?php echo $cupon_10_descuento; ?>" value="<?php echo $cupon_10_descuento; ?>">
				<a class="punt_cupon btn btn-primary" href="#">10 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<!-- <a class="btn btn-primary" href="https://www.tiendasmk.com/cupones-por-10-puntos-referidos/">10 puntos</a> -->
				<!-- Id del Afiliado -->
				<!-- Id del Afiliado -->
				<!-- Puntos a Utilizar -->
				<!-- Puntos Distribuidores Referidos -->
				<!-- Puntos Cupones Descuento -->
				<!-- Cupon por Boton-->
				<input class="id_affiliate" type="hidden" name="id_affiliate<?php echo $affiliate_id; ?>" value="<?php echo $affiliate_id; ?>">
				<input class="punt_use" type="hidden" name="punt_use<?php echo $cupon20; ?>" value="<?php echo $cupon20; ?>">
				<input class="punt_dist" type="hidden" name="punt_dist<?php echo $monto; ?>" value="<?php echo $monto; ?>">
				<input class="punt_cupDesc" type="hidden" name="punt_cupDesc<?php echo $puntos_sum; ?>" value="<?php echo $puntos_sum; ?>">
				<input class="punt_usados" type="hidden" name="punt_usados<?php echo $usados; ?>" value="<?php echo $usados; ?>">
				<input class="cupon" type="hidden" name="cupon<?php echo $cupon_20_descuento; ?>" value="<?php echo $cupon_20_descuento; ?>">
				<a class="punt_cupon btn btn-primary" href="#">20 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<!-- <a class="btn btn-primary" href="https://www.tiendasmk.com/cupones-por-10-puntos-referidos/">10 puntos</a> -->
				<!-- Id del Afiliado -->
				<!-- Id del Afiliado -->
				<!-- Puntos a Utilizar -->
				<!-- Puntos Distribuidores Referidos -->
				<!-- Puntos Cupones Descuento -->
				<!-- Cupon por Boton-->
				<input class="id_affiliate" type="hidden" name="id_affiliate<?php echo $affiliate_id; ?>" value="<?php echo $affiliate_id; ?>">
				<input class="punt_use" type="hidden" name="punt_use<?php echo $cupon30; ?>" value="<?php echo $cupon30; ?>">
				<input class="punt_dist" type="hidden" name="punt_dist<?php echo $monto; ?>" value="<?php echo $monto; ?>">
				<input class="punt_cupDesc" type="hidden" name="punt_cupDesc<?php echo $puntos_sum; ?>" value="<?php echo $puntos_sum; ?>">
				<input class="punt_usados" type="hidden" name="punt_usados<?php echo $usados; ?>" value="<?php echo $usados; ?>">
				<input class="cupon" type="hidden" name="cupon<?php echo $cupon_30_descuento; ?>" value="<?php echo $cupon_30_descuento; ?>">
				<a class="punt_cupon btn btn-primary" href="#">30 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<!-- <a class="btn btn-primary" href="https://www.tiendasmk.com/cupones-por-10-puntos-referidos/">10 puntos</a> -->
				<!-- Id del Afiliado -->
				<!-- Id del Afiliado -->
				<!-- Puntos a Utilizar -->
				<!-- Puntos Distribuidores Referidos -->
				<!-- Puntos Cupones Descuento -->
				<!-- Cupon por Boton-->
				<input class="id_affiliate" type="hidden" name="id_affiliate<?php echo $affiliate_id; ?>" value="<?php echo $affiliate_id; ?>">
				<input class="punt_use" type="hidden" name="punt_use<?php echo $cupon40; ?>" value="<?php echo $cupon40; ?>">
				<input class="punt_dist" type="hidden" name="punt_dist<?php echo $monto; ?>" value="<?php echo $monto; ?>">
				<input class="punt_cupDesc" type="hidden" name="punt_cupDesc<?php echo $puntos_sum; ?>" value="<?php echo $puntos_sum; ?>">
				<input class="punt_usados" type="hidden" name="punt_usados<?php echo $usados; ?>" value="<?php echo $usados; ?>">
				<input class="cupon" type="hidden" name="cupon<?php echo $cupon_40_descuento; ?>" value="<?php echo $cupon_40_descuento; ?>">
				<a class="punt_cupon btn btn-primary" href="#">40 puntos</a>
			</div>
		</div>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="wrap-buttons">
				<!-- <a class="btn btn-primary" href="https://www.tiendasmk.com/cupones-por-10-puntos-referidos/">10 puntos</a> -->
				<!-- Id del Afiliado -->
				<!-- Id del Afiliado -->
				<!-- Puntos a Utilizar -->
				<!-- Puntos Distribuidores Referidos -->
				<!-- Puntos Cupones Descuento -->
				<!-- Cupon por Boton-->
				<input class="id_affiliate" type="hidden" name="id_affiliate<?php echo $affiliate_id; ?>" value="<?php echo $affiliate_id; ?>">
				<input class="punt_use" type="hidden" name="punt_use<?php echo $cupon50; ?>" value="<?php echo $cupon50; ?>">
				<input class="punt_dist" type="hidden" name="punt_dist<?php echo $monto; ?>" value="<?php echo $monto; ?>">
				<input class="punt_cupDesc" type="hidden" name="punt_cupDesc<?php echo $puntos_sum; ?>" value="<?php echo $puntos_sum; ?>">
				<input class="punt_usados" type="hidden" name="punt_usados<?php echo $usados; ?>" value="<?php echo $usados; ?>">
				<input class="cupon" type="hidden" name="cupon<?php echo $cupon_50_descuento; ?>" value="<?php echo $cupon_50_descuento; ?>">
				<a class="punt_cupon btn btn-primary" href="#">50 puntos</a>
			</div>
		</div>
		<?php endif;?>
	<?php endif; ?>
	</div>
    <div class="vc_empty_space" style="height: 40px"><span class="vc_empty_space_inner"></span></div>
    
	<?php
	/**
	 * Fires after the data table within the affiliate area referrals template.
	 *
	 * @param int $affiliate_id Affiliate ID.
	 */
	do_action( 'affwp_referrals_dashboard_after_table', $affiliate_id );
	?>

	<?php /*if ( $pages > 1 ) : ?>

		<p class="affwp-pagination">
			<?php
			echo paginate_links(
				array(
					'current'      => $page,
					'total'        => $pages,
					'add_fragment' => '#affwp-affiliate-dashboard-referrals',
					'add_args'     => array(
						'tab' => 'referrals',
					),
				)
			);
			?>
		</p>
	<?php endif;*/ ?>
</div>
