﻿=== GamiPress ===
Contributors: gamipress, tsunoa, rubengc, eneribs
Tags: gamipress, gamification, points, achievements, ranks, badges, awards, rewards, credits, engagement, leaderboard, emails, notifications, progress
Requires at least: 4.4
Tested up to: 5.3
Stable tag: 1.7.8.1
License: GNU AGPL v3.0
License URI: http://www.gnu.org/licenses/agpl-3.0.html

The most flexible and powerful gamification system for WordPress.

== Description ==

[GamiPress](https://gamipress.com "GamiPress") is the easiest way to gamify your WordPress website in just a few minutes, letting you award your users with digital rewards for interacting with your site.

Easily define the achievements, organize requirements, and choose from a range of assessment options to determine whether each task or requirement has been successfully achieved.

GamiPress is extremely powerful and infinitely extensible. Check out some of the built in features:

= 3 powerful ways to award your users =

GamiPress combines three of the most powerful award systems you could add to your site:

* **Points** to automatically award your users for interacting with your site.
* **Achievements** to award users for completing all the requirements, sequentially or otherwise.
* **Ranks** to let your users climb through the ranks by completing all the rank requirements.

= Unlimited ways to define how to award the different points, achievements and ranks =

* Site activity (events based on publishing posts and pages, commenting, daily visits or logging in to your site).
* Completing specific other achievements, once or a specified number of times.
* Completing one or all achievements of a specified type.
* Points thresholds.
* Admin awarded achievements.
* Reaching a specific rank.
* Expending an amount of points.

= Features =

* **Points Types:** Configure as many types of points as you like (Credits, Gems, Coins, etc).
* **Achievement Types:** Configure as many types of achievement as you like (Quests, Badges, etc).
* **Rank Types:** Configure as many types of rank as you like (Level, Grade, etc).
* **Automatic points awards and deductions:** Easily configure automatic ways to award or deduct points to your users.
* **Achievement steps:** Define conditional steps, thresholds and more.
* **Rank requirements:** Define conditional requirements to reach any rank.
* **Time limit requirements:** Limit by time period when the user can complete a specific requirement (daily, weekly, monthly or yearly).
* **Drag and drop controls:** Powerful controls to setup your gamification environment in minutes.
* **Emails:** Your users will get notified automatically about new awards.
* **Logs:** Flexible log system with support for public and private logs.
* **Unlock achievements and ranks using points:** Let users to optionally unlock any achievement or rank by expending an amount of points without meet the requirements.
* **Gutenberg blocks:** Built-in support for Gutenberg including a great number of blocks to place them anywhere.
* **Shortcodes & Widgets:** WordPress-friendly shortcodes and widgets to show the user points wallet, earned achievements, latest logs, and more.
* **Live shortcode editor:** Missing a shortcode parameter? Just press the "GamiPress Shortcode" button and set up any shortcode without a worry.
* **GDPR Support:** Support for WordPress personal data exports and deletions.
* **Theme Agnostic:** GamiPress works with just about any standard WordPress theme. No special hooks or theme updates are needed.
* **Templates System:** Overwritable templates system to allow you customize everything you want through your GamiPress theme folder.
* **Data centralization on Multisite:** Centralize all the data on multisite installs and show anything you want on any sub-site.
* **Rest API:** Full support to WordPress rest API brings you new ways to connect GamiPress with external applications.
* **Developer-friendly:** GamiPress is extremely flexible with plenty of hooks to add custom features and functionalities.

= Integrated with your favorites WordPress plugins =

GamiPress integrates with a large number of plugins allowing you to add gamification in any environment.

= LMS integrations =

* [LearnDash](https://wordpress.org/plugins/gamipress-learndash-integration/)
* [H5P](https://wordpress.org/plugins/gamipress-h5p-integration/)
* [LearnPress](https://wordpress.org/plugins/gamipress-learnpress-integration/)
* [LifterLMS](https://wordpress.org/plugins/gamipress-lifterlms-integration/)
* [CoursePress](https://wordpress.org/plugins/gamipress-coursepress-integration/)
* [Tutor LMS](https://wordpress.org/plugins/gamipress-tutor-integration/)
* [Sensei](https://wordpress.org/plugins/gamipress-sensei-integration/)
* [WPLMS](https://wordpress.org/plugins/gamipress-wplms-integration/)
* [WPEP](https://wordpress.org/plugins/gamipress-wpep-integration/)
* [WP Courseware](https://wordpress.org/plugins/gamipress-wp-courseware-integration/)

= Forms integrations =

* [Ninja Forms](https://wordpress.org/plugins/gamipress-ninja-forms-integration/)
* [WP Forms](https://wordpress.org/plugins/gamipress-wp-forms-integration/)
* [Formidable Forms](https://wordpress.org/plugins/gamipress-formidable-forms-integration/)
* [Forminator](https://wordpress.org/plugins/gamipress-forminator-integration/)
* [Contact Form 7](https://wordpress.org/plugins/gamipress-contact-form-7-integration/)
* [Gravity Forms](https://wordpress.org/plugins/gamipress-gravity-forms-integration/)
* [Caldera Forms](https://wordpress.org/plugins/gamipress-caldera-forms-integration/)

= Other integrations =

* [Easy Digital Downloads](https://wordpress.org/plugins/gamipress-easy-digital-downloads-integration/)
* [WooCommerce](https://wordpress.org/plugins/gamipress-woocommerce-integration/)
* [AffiliateWP](https://wordpress.org/plugins/gamipress-affiliatewp-integration/)
* [BuddyPress](https://wordpress.org/plugins/gamipress-buddypress-integration/)
* [bbPress](https://wordpress.org/plugins/gamipress-bbpress-integration/)
* [Jetpack](https://wordpress.org/plugins/gamipress-jetpack-integration/)
* [Youtube](https://wordpress.org/plugins/gamipress-youtube-integration/)
* [Vimeo](https://wordpress.org/plugins/gamipress-vimeo-integration/)
* [WP Job Manager](https://wordpress.org/plugins/gamipress-wp-job-manager-integration/)
* [Ultimate Member](https://wordpress.org/plugins/gamipress-ultimate-member-integration/)
* [Give](https://wordpress.org/plugins/gamipress-give-integration/)
* [PeepSo](https://wordpress.org/plugins/gamipress-peepso-integration/)
* [The Events Calendar](https://wordpress.org/plugins/gamipress-the-events-calendar-integration/)
* [Events Manager](https://wordpress.org/plugins/gamipress-events-manager-integration/)
* [Simple:Press](https://wordpress.org/plugins/gamipress-simplepress-integration/)
* [wpForo](https://wordpress.org/plugins/gamipress-wpforo-integration/)
* [Invite Anyone](https://wordpress.org/plugins/gamipress-invite-anyone-integration/)
* [WP Ulike](https://wordpress.org/plugins/gamipress-wp-ulike-integration/)
* [Favorites](https://wordpress.org/plugins/gamipress-favorites-integration/)
* [WP Polls](https://wordpress.org/plugins/gamipress-wp-polls-integration/)
* [WP PostRatings](https://wordpress.org/plugins/gamipress-wp-postratings-integration/)

[View all integrations](https://gamipress.com/integrations/)

= Powerful add-ons to extend GamiPress =

WordPress.org is home to some amazing extensions for GamiPress, including:

* [Transfers Notes](https://wordpress.org/plugins/gamipress-transfers-notes/)
* [Conditional Emails Recipients](https://wordpress.org/plugins/gamipress-conditional-emails-recipients/)
* [Leaderboards Include/Exclude Users](https://wordpress.org/plugins/gamipress-leaderboards-include-exclude-users/)
* [LearnDash Group Leaderboard](https://wordpress.org/plugins/gamipress-learndash-group-leaderboard/)
* [BuddyPress Group Leaderboard](https://wordpress.org/plugins/gamipress-buddypress-group-leaderboard/)
* [PeepSo Group Leaderboard](https://wordpress.org/plugins/gamipress-peepso-group-leaderboard/)
* [Block Users](https://wordpress.org/plugins/gamipress-block-users/)
* [Notifications By Type](https://wordpress.org/plugins/gamipress-notifications-by-type/)
* [Emails By Type](https://wordpress.org/plugins/gamipress-emails-by-type/)
* [Multimedia Content](https://wordpress.org/plugins/gamipress-multimedia-content/)
* [Link](https://wordpress.org/plugins/gamipress-link/)
* [Activity by Category](https://wordpress.org/plugins/gamipress-activity-by-category/)

If you’re looking for something endorsed and maintained by the developers who built GamiPress, there are a plethora of premium add-ons, the most popular of which include:

* [Time-based Rewards](https://gamipress.com/add-ons/gamipress-time-based-rewards/)
* [Frontend Reports](https://gamipress.com/add-ons/gamipress-frontend-reports/)
* [Restrict Unlock](https://gamipress.com/add-ons/gamipress-restrict-unlock/)
* [Conditional Emails](https://gamipress.com/add-ons/gamipress-conditional-emails/)
* [Conditional Notifications](https://gamipress.com/add-ons/gamipress-conditional-notifications/)
* [Email Digests](https://gamipress.com/add-ons/gamipress-email-digests/)
* [Rest API Extended](https://gamipress.com/add-ons/gamipress-rest-api-extended/)
* [Referrals](https://gamipress.com/add-ons/gamipress-referrals/)
* [Coupons](https://gamipress.com/add-ons/gamipress-coupons/)
* [Points Exchanges](https://gamipress.com/add-ons/gamipress-points-exchanges/)
* [Transfers](https://gamipress.com/add-ons/gamipress-transfers/)
* [Daily Login Rewards](https://gamipress.com/add-ons/gamipress-daily-login-rewards/)
* [Reports](https://gamipress.com/add-ons/gamipress-reports/)
* [Purchases](https://gamipress.com/add-ons/gamipress-purchases/)
* [Restrict Content](https://gamipress.com/add-ons/gamipress-restrict-content/)
* [Notifications](https://gamipress.com/add-ons/gamipress-notifications/)
* [Progress Map](https://gamipress.com/add-ons/gamipress-progress-map/)
* [Progress](https://gamipress.com/add-ons/gamipress-progress/)
* [Leaderboards](https://gamipress.com/add-ons/gamipress-leaderboards/)
* [Social Share](https://gamipress.com/add-ons/gamipress-social-share/)

[View all add-ons](https://gamipress.com/add-ons/)

= Helpful Links =

* [GamiPress.com](https://gamipress.com/ "GamiPress") - Official Website
* [Add-ons](https://gamipress.com/add-ons "GamiPress Add-ons") - Official Add-ons
* [Assets](https://gamipress.com/assets "GamiPress Assets") - Official Assets
* [Documentation](https://gamipress.com/docs "GamiPress Documentation") - Official Documentation
* [Code Snippets](https://gamipress.com/customize "GamiPress Code Snippets") - Official Code Snippets
* [Contact](https://gamipress.com/contact-us "GamiPress Contact") - Contact Details
* [GitHub](https://github.com/rubengc/gamipress "GamiPress on GitHub") - GitHub Repository

== Installation ==

= From WordPress backend =

1. Navigate to Plugins -> Add new.
2. Click the button "Upload Plugin" next to "Add plugins" title.
3. Upload the downloaded zip file and activate it.

= Direct upload =

1. Upload the downloaded zip file into your `wp-content/plugins/` folder.
2. Unzip the uploaded zip file.
3. Navigate to Plugins menu on your WordPress admin area.
4. Activate this plugin.

== Screenshots ==

1. A huge number of shortcodes and widgets with a lot of display options.
2. Configure as many points types as you like: Credits, Gems, Coins, etc.
3. Configure as many achievement types as you like: Badges, Quests, etc.
4. Configure as many rank types as you like: Grade, Level, etc.
5. Let users to optionally unlock any achievement or rank by expending an amount of points without meet the requirements.
6. Simple yet powerful admin interface to manage your gamification elements.
7. Drag and drop control to define the requirements for any achievement, points type or rank.
8. Built-in support for Gutenberg including a great number of blocks to place them anywhere.
9. Live shortcode editor appears in the toolbar of all WordPress content editor areas, allowing you to transform any page or post into part of your gamification system without referencing any of the shortcodes.
10. WordPress-friendly widgets to show the user points wallet, earned achievements, latest logs, and more.
11. Configurable email templates to let your users get notified automatically about new awards.
12. Flexible log system with support for public and private logs.

== Frequently Asked Questions ==

= Is GamiPress compatible with any theme? =

We built GamiPress so that it will work with modern WordPress themes. GamiPress just adds positional styles which will allow you to better customize everything for your specific needs.

= Can GamiPress be easily translated? =

Yes, GamiPress is stored in the official WordPress plugins repository where you (and anyone) are able to [submit your own translations](https://translate.wordpress.org/projects/wp-plugins/gamipress).

= Does GamiPress work with WordPress multisite? =

Yes. You can use GamiPress on a WordPress multisite network.

In addition, GamiPress has the ability to centralize all the data when is [network wide active](https://gamipress.com/docs/advanced/multisite/).

= Does GamiPress work with WordPress rest API? =

Yes. GamiPress includes full built-in support to the WordPress rest API.

You can find all information about rest API [on this page](https://gamipress.com/docs/advanced/rest-api/).

= Where can I find documentation about GamiPress? =

Check the [Getting Started](https://gamipress.com/docs/getting-started/) docs where you can start getting familiarized with GamiPress.

Also, we have the [tutorials](https://gamipress.com/docs/tutorials/) sections where you can find step-by-step guides to accomplish some common task to start working with GamiPress.

= Where can I find code snippets to customize GamiPress? =

Check [our customize section](https://gamipress.com/customize/) where you can find a huge number of code snippets to help you customize GamiPress.

= Where can I find images to customize the GamiPress elements? =

Check [our assets section](https://gamipress.com/assets/) where you can find a huge number of resources to decorate your gamification elements to take the design of them to the next level.

= Which shortcodes come bundled with GamiPress? =

GamiPress comes with the following shortcodes:

* [[gamipress_achievement]](https://gamipress.com/docs/shortcodes/gamipress_achievement/) to display a desired achievement.
* [[gamipress_achievements]](https://gamipress.com/docs/shortcodes/gamipress_achievements/) to display a list of achievements.
* [[gamipress_earnings]](https://gamipress.com/docs/shortcodes/gamipress_earnings/) to display a list of user earnings.
* [[gamipress_logs]](https://gamipress.com/docs/shortcodes/gamipress_logs/) to display a list of logs.
* [[gamipress_points_types]](https://gamipress.com/docs/shortcodes/gamipress_points_types/) to display a list of points types with their points awards and deducts.
* [[gamipress_points]](https://gamipress.com/docs/shortcodes/gamipress_points/) to display current or specific user points balance.
* [[gamipress_rank]](https://gamipress.com/docs/shortcodes/gamipress_rank/) to display a desired rank.
* [[gamipress_ranks]](https://gamipress.com/docs/shortcodes/gamipress_ranks/) to display a list of ranks.
* [[gamipress_user_rank]](https://gamipress.com/docs/shortcodes/gamipress_user_rank/) to display previous, current and/or next rank of an user.

In your WordPress admin area, navigate to the GamiPress Help/Support menu where you can find the full list of available shortcodes, including descriptions of all parameters each shortcode supports.

= Which widgets come bundled with GamiPress? =

GamiPress comes with the following widgets:

* Achievement: to display a desired achievement.
* Achievements: to display a list of achievements.
* User Earnings: to display a list of user earnings.
* Logs: to display a list of logs.
* Points Types: to display a list of points types with their points awards and deducts.
* User Points: to display current or specific user points balance.
* Rank: to display a desired rank.
* Ranks: to display a list of ranks.
* User Rank: to display previous, current and/or next rank of an user.

= Do you offer custom development services? =

No, We're unable to provide custom development services, as our focus is developing the core GamiPress plugin, and the official GamiPress add-ons. If you need customization services check our [customizations page](https://gamipress.com/customizations/).

Also, you can check [our customize section](https://gamipress.com/customize/) where you can find a huge number of code snippets to help you customize GamiPress!

== Changelog ==

= 1.7.8.1 =

* **New Features**
* Added confirmation on unlock achievement and rank using points.
* **Improvements**
* Code reduction on unlock achievement and rank using points code.
* **Developer Notes**
* Full refactor of unlock achievement and rank using points code.
* Added hooks to deactivate confirmation on unlock achievement and rank using points.

= 1.7.8 =

* **New Features**
* Support to WordPress 5.3 admin style guidelines.
* **Improvements**
* Make GamiPress switches match WordPress togglers style.
* Make GamiPress Select2 match WordPress selects style.

= 1.7.7 =

* **New Features**
* Added the fields "Include" and "Exclude" to the Earnings block and widget.
* Added the attributes "include" and "exclude" to the [gamipress_earnings] shortcodes.
* Added support to award by roles to the Bulk Award Tool.
* Added support to revoke by roles to the Bulk Revokes Tool.
* **Improvements**
* Added some extra checks on shortcodes editor to avoid warnings while looping shortcode groups.
* Readme reduction by removing the add-ons descriptions.
* Avoid conflicts on logs compatibility functions.
* Removed gamipress-blocks-style.css file since is not in use.
* Added site ID metadata on logs when GamiPress is network wide active on multisite.
* Performance improvements on multisites by caching queried posts on rules engine.
* Code improvements for bulk awards and bulk revokes scripts.
* **Bug Fixes**
* Fixed an issue that makes ranks not being awarded on multisites when user meets it's requirements on a subsite.
* **Developer Notes**
* Added Javascript events before and after unlock achievements and ranks through points.
* Updated Select2 library to 4.0.10.
* Creation of the gamipress_select2() to ensure load always the up to date select2 version.
* Replacement of all select2() calls to gamipress_select2().
* Removed select2 information on System Info tool.

= 1.7.6.3 =

* **Bug Fixes**
* Fixed detection of points earned when awards engine awards a requirement with "Earn an amount of points" event multiple times.

= 1.7.6.2 =

* **Bug Fixes**
* Fixed issue on Gutenberg blocks conditions functionality when passing an array to define the condition.
* Fixed conflicts with WordPress javascript libraries by updating Gutenberg blocks dependencies.
* **Improvements**
* Added support for Javascript objects conditions on Gutenberg blocks.
* Updated Gutenberg blocks dependencies.

= 1.7.6.1 =

* **Improvements**
* Update blocks code to avoid conflicts with others plugins.
* Remove lodash usage to avoid functions conflicts.
* Updated all Gutenberg blocks dependencies to latest stable releases.

= 1.7.6 =

* **New Features**
* Added database information on System Info tool.
* Added support for shortcode groups on Shortcodes Editor.
* **Bug Fixes**
* Prevent to take field values from repeatable patterns on Shortcodes Editor.
* **Improvements**
* Style improvements for repeatable color pickers on Shortcodes Editor.
* Improvements on shortcode error messages.
* Added the ability to detect if an error is rendered on a shortcode, block or widget.
* **Developer Notes**
* Added support for multicheck fields on Shortcodes Editor.
* Added support to multiples selects on query logs function.
* Added support to group_by to query logs function.
* Added support to output parameter on query logs function.
* New Gutenberg component: MultiCheckboxControl (works equal than RadioControl but with checkboxes).
* Added support for multicheck fields through MultiCheckboxControl on Gutenberg blocks.

= 1.7.5 =

* **Bug Fixes**
* Fixed detection of points earned when awards engine awards a requirement with "Earn an amount of points" event multiple times.
* Fixed Javascript assets load on user edit screen when a third party plugin creates an editor on this screen.
* **Improvements**
* Performance improvements on last points awarded detection.
* On rename a points type update logs metas and user earnings with old points type.
* Added support to multiples conditional field values on Gutenberg fields.
* **Developer Notes**
* Increased GamiPress plugins API transient expiration time from 24 to 48 hours.

= 1.7.4.2 =

* **Bug Fixes**
* Fixed type on [gamipress_points_types] shortcode slug used to pair attributes.
* **Improvements**
* Improved user ID detection on user profile form included when other plugins causes issues on this form to keep GamiPress utilities working.
* Prevent to display all user earnings to visitors if current user is set to "yes".
* **Developer Notes**
* Added filters to change the workflow on user earnings and logs shortcode to display or not content to visitors when current user is set to "yes".

= 1.7.4.1 =

* **Bug Fixes**
* Fixed admin scripts localization to avoid issues when scripts are loaded on not expected pages.

= 1.7.4 =

* **Bug Fixes**
* Fixed blocks single post/user fields not saving.
* Fixed incorrect posts on requirements UI posts selector when GamiPress is network wide active.
* **Improvements**
* Prevent type's slug match any WordPress reserved term.
* Full reformat of slug check function.

= 1.7.3 =

* **New Features**
* New event: Get a comment marked as spam.
* New event: Get a comment of a specific post marked as spam.
* **Improvements**
* Improved "Daily visit any post" event count detection.
* **Developer Notes**
* GamiPress fully tested up to WordPress 5.2.


= 1.7.2 =

* **Bug Fixes**
* Fixed required times check from last update checking just last time earned when needed (and not for all checks).
* Fixed GamiPress dropdowns display on customizer.

= 1.7.1 =

* **Bug Fixes**
* Fixed required times check on steps that achievement can be earned multiples times.
* Fixed requirements connected to column (just visible if debug mode is enabled).
* **Developer Notes**
* Added new helper functions.
* Moved functions on new files to improve project organization.

= 1.7.0 =

* **New Features**
* Added a new tool named "Import/Export Setup" that allows import/export GamiPress setup (points types, achievements, steps, ranks, etc).
* Added the ability to Import/Export Setup tool to import attachments downloading them on directly to the server, if possible.
* Added the ability to deduct points, revoke achievements or ranks through the CSV import tool by adding the negative sign "-".
* **Bug Fixes**
* Prevent to display removed or unpublished achievements on gamipress_get_user_achievements() function.
* Prevent to add empty shortcode attributes on through the shortcodes editor.
* **Improvements**
* Added a confirmation message when revoking an user earning.
* Improvements on the Widgets API that handles much better widget setup of external fields.
* Style improvements on tools and settings screens.
* Updated MySQL minimum requirement to meet WordPress recommendations.
* Added more information to the System Info tool.
* Update user assigned ranks and points balances when manually award or revoke anything.
* **Developer Notes**
* Added helper functions to easily build a shortcode attributes array based on values given.
* Updated plugin updater class for non wordpress.org plugins.
* Reset public changelog (moved old changelog to changelog.txt file).
* Set GamiPress 1.7.0 as new stable release! :)