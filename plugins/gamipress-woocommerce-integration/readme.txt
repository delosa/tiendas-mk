=== GamiPress - WooCommerce integration ===
Contributors: gamipress, tsunoa, rubengc, eneribs
Tags: ecommerce, store, gamipress, gamification, points, achievements, badges, awards, rewards, credits, engagement, woocommerce, wc, product
Requires at least: 4.4
Tested up to: 5.2
Stable tag: 1.2.0
License: GNU AGPLv3
License URI: http://www.gnu.org/licenses/agpl-3.0.html

Connect GamiPress with WooCommerce

== Description ==

Gamify your [WooCommerce](http://wordpress.org/plugins/woocommerce/ "WooCommerce") store thanks to the powerful gamification plugin, [GamiPress](https://wordpress.org/plugins/gamipress/ "GamiPress")!

This plugin automatically connects GamiPress with WooCommerce adding new activity events and features.

= New Events =

* New product: When an user publish a new product.
* New purchase: When an user makes a new purchase (triggered 1 time on a purchase, independent of the number of products purchased).
* New product purchase: When an user purchases a product (triggered on every product purchased on a purchase).
* Specific product purchase: When an user purchases a specific product.
* Specific product variation purchase: When an user purchases a specific product variation.
* New product purchase of a specific category: When an user purchases a product of a specific category.
* New product purchase of a specific tag: When an user purchases a product of a specific tag.
* Review a product: When an user reviews a product.
* Review a specific product: When an user reviews a specific product.
* New sale: When an user (vendor) makes a new sale.
* Refund a purchase: When an user refunds a purchase.
* Refund a product: When an user refunds a product (triggered on every product refunded on a purchase).
* Refund a specific product: When an user purchases a specific product.
* Refund a specific product variation: When an user refunds a specific product variation.
* Refund a product of a specific category: When an user refunds a product of a specific category.
* Refund a product of a specific tag: When an user refunds a product of a specific tag.
* Get a new refund: When an user (vendor) gets a product refunded.

= New Features =

* From the product edit screen you will be able to setup an amount of points to award customers for purchase it.

= Expand your WooCommerce integration =

There are more add-ons that improves your experience with GamiPress and WooCommerce:

* [WooCommerce Points Gateway](https://gamipress.com/add-ons/gamipress-wc-points-gateway/)
* [WooCommerce Discounts](https://gamipress.com/add-ons/gamipress-wc-discounts/)

== Installation ==

= From WordPress backend =

1. Navigate to Plugins -> Add new.
2. Click the button "Upload Plugin" next to "Add plugins" title.
3. Upload the downloaded zip file and activate it.

= Direct upload =

1. Upload the downloaded zip file into your `wp-content/plugins/` folder.
2. Unzip the uploaded zip file.
3. Navigate to Plugins menu on your WordPress admin area.
4. Activate this plugin.

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==

= 1.2.0 =

* **Improvements**
* Avoid to grant access to any event on duplicity checks.
* Moved old changelog to changelog.txt file.
