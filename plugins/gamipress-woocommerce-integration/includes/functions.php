<?php
/**
 * Functions
 *
 * @package GamiPress\WooCommerce\Functions
 * @since 1.0.0
 */
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

/**
 * Award user with points on purchase
 *
 * @param integer $order_id
 */
function gamipress_wc_maybe_award_points_on_purchase( $order_id ) {

    $order = wc_get_order( $order_id );

    if( $order ) {

        $items = $order->get_items();

        if ( is_array( $items ) ) {

            // Loop each cart item to check if someone awards points to the user
            foreach ( $items as $item ) {

                $product_id = 0;
                $quantity = 1;

                // Depending of the WooCommerce version, items could be an array or an WC_Order_Item object
                if( class_exists( 'WC_Order_Item' ) && $item instanceof WC_Order_Item ) {
                    $product_id = $item->get_product_id();
                    $quantity = $item->get_quantity();
                } else if( is_array( $item ) && isset( $item['product_id'] ) ) {
                    $product_id = $item['product_id'];
                    $quantity = isset( $item['qty'] ) ? $item['qty'] : $item['quantity'];
                }

                if( $product_id !== 0 && $quantity > 0 ) {

                    // Check if product was setup to award points
                    $user_id = $order->get_user_id();
                    $award_points = (bool) get_post_meta( $product_id, '_gamipress_wc_award_points', true );

                    /**
                     * Filter to check if is available to award points for purchase
                     *
                     * @since 1.1.2
                     *
                     * @param bool                  $award_points   Whatever if should points be awarded to the user or not
                     * @param int                   $user_id        The user ID that will be awarded
                     * @param int                   $product_id     The product ID that user has purchased
                     * @param int                   $order_id       The order ID
                     * @param WC_Order_Item|array   $item           The order item object
                     */
                    $award_points = apply_filters( 'gamipress_wc_award_points_for_purchase', $award_points, $user_id, $product_id, $order_id, $item );

                    if( $award_points ) {

                        $product = wc_get_product( $product_id );

                        // Get the amount of points to award and the points type
                        $points = absint( get_post_meta( $product_id, '_gamipress_wc_points', true ) );
                        $points_type = get_post_meta( $product_id, '_gamipress_wc_points_type', true );

                        // The amount to award if based to the quantity added to the cart
                        $points = absint( $points * $quantity );

                        // Setup the custom reason for the log
                        if( $quantity === 1 ) {
                            $reason =  sprintf(
                                __( '%d %s awarded for purchase %s', 'gamipress-woocommerce-integration' ),
                                $points,
                                gamipress_get_points_type_plural( $points_type ),
                                $product->get_name()
                            );
                        } else {
                            $reason =  sprintf(
                                __( '%d %s awarded for purchase %d %s', 'gamipress-woocommerce-integration' ),
                                $points,
                                gamipress_get_points_type_plural( $points_type ),
                                $quantity,
                                $product->get_name()
                            );
                        }

                        // Setup the points award args
                        $args = array(
                            'reason' => $reason,
                            'log_type' => 'points_earn',
                        );

                        // Award the points to the user
                        gamipress_award_points_to_user( $user_id, $points, $points_type, $args );

                    }

                }

            }

        }

    }

}
add_action( 'woocommerce_payment_complete', 'gamipress_wc_maybe_award_points_on_purchase' );

// Helper function to get product variations
function gamipress_wc_get_product_variations( $product_id ) {

    $product = wc_get_product( $product_id );

    // Bail if product doesn't exists
    if( ! $product ) {
        return array();
    }

    // Bail if product is not variable
    if( ! $product->is_type( 'variable' ) ) {
        return array();
    }

    $available_variations = $product->get_available_variations();

    // Bail if there isn't any variations
    if( ! is_array( $available_variations )  ) {
        return array();
    }

    // Return product variations
    return $available_variations;

}

// Helper function to get product variations dropdown
function gamipress_wc_get_product_variations_dropdown( $product_id, $selected = 0 ) {

    $variations = gamipress_wc_get_product_variations( $product_id );

    if( empty( $variations ) ) {
        return '';
    }

    $output = '<select>';

    foreach( $variations as $variation ) {

        $attributes = array();

        foreach( $variation['attributes'] as $attribute ) {
            if( ! empty( $attribute ) ) {
                $attributes[] = $attribute;
            }
        }

        $output .= '<option value="' . $variation['variation_id'] . '" ' . selected( $selected, $variation['variation_id'], false ) . '>' . implode( ', ', $attributes ) . ' (#' . $variation['variation_id'] . ')</option>';
    }

    $output .= '</select>';

    return $output;

}

// Helper function to get product variation title
function gamipress_wc_get_product_variation_title( $product_id, $variation_id ) {

    $variation_attributes = wc_get_product_variation_attributes( $variation_id );
    $attributes = array();

    foreach( $variation_attributes as $attribute ) {
        if( ! empty( $attribute ) ) {
            $attributes[] = $attribute;
        }
    }

    return get_post_field( 'post_title', $product_id )
            . ( ! empty( $attributes ) ? ' (' . implode( ', ', $attributes ) . ')' : '' );

}