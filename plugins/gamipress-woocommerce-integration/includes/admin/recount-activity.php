<?php
/**
 * Recount Activity
 *
 * @package GamiPress\WooCommerce\Admin\Recount_Activity
 * @since 1.0.4
 */
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

/**
 * Add recountable options to the Recount Activity Tool
 *
 * @since 1.0.4
 *
 * @param array $recountable_activity_triggers
 *
 * @return array
 */
function gamipress_wc_recountable_activity_triggers( $recountable_activity_triggers ) {

    $recountable_activity_triggers[__( 'WooCommerce', 'gamipress-woocommerce-integration' )] = array(
        'wc_purchases' => __( 'Recount purchases', 'gamipress-woocommerce-integration' ),
        'wc_reviews' => __( 'Recount reviews', 'gamipress-woocommerce-integration' ),
    );

    return $recountable_activity_triggers;

}
add_filter( 'gamipress_recountable_activity_triggers', 'gamipress_wc_recountable_activity_triggers' );

/**
 * Recount purchases
 *
 * @since 1.0.4
 *
 * @param array $response
 *
 * @return array $response
 */
function gamipress_wc_activity_recount_purchases( $response ) {

    global $wpdb;

    // Get all stored users
    $users = $wpdb->get_results( "SELECT {$wpdb->users}.ID FROM {$wpdb->users}" );

    foreach( $users as $user ) {
        // Get all completed orders
        $orders = wc_get_orders( array(
            'limit'    => -1,
            'status'   => 'completed',
            'customer' => $user->ID,
            'return' => 'ids',
        ) );

        foreach( $orders as $order_id ) {
            // Trigger new purchase action for each payment
            gamipress_wc_new_purchase( $order_id );
        }

    }

    return $response;

}
add_filter( 'gamipress_activity_recount_wc_purchases', 'gamipress_wc_activity_recount_purchases' );

/**
 * Recount reviews
 *
 * @since 1.0.4
 *
 * @param array $response
 *
 * @return array $response
 */
function gamipress_wc_activity_recount_reviews( $response ) {

    global $wpdb;

    // Get all stored users
    $users = $wpdb->get_results( "SELECT {$wpdb->users}.ID FROM {$wpdb->users}" );

    foreach( $users as $user ) {
        // Get all user approved reviews
        $comments = $wpdb->get_results( $wpdb->prepare(
            "
            SELECT *
            FROM $wpdb->comments AS c
            WHERE c.user_id = %s
		       AND c.comment_approved = '1'
		       AND c.comment_type = 'review'
            ",
            $user->ID
        ) );

        foreach( $comments as $comment ) {
            // Trigger review actions
            do_action( 'gamipress_wc_specific_new_review', (int) $comment->ID, (int) $comment->user_id, $comment->comment_post_ID, $comment );
            do_action( 'gamipress_wc_new_review', (int) $comment->ID, (int) $comment->user_id, $comment->comment_post_ID, $comment );
        }
    }

    return $response;

}
add_filter( 'gamipress_activity_recount_wc_reviews', 'gamipress_wc_activity_recount_reviews' );