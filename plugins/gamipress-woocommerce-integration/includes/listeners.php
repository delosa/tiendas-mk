<?php
/**
 * Listeners
 *
 * @package GamiPress\WooCommerce\Listeners
 * @since 1.0.0
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

// Purchase listener
function gamipress_wc_new_purchase( $order_id ) {

    $order = wc_get_order( $order_id );
    $user_id = $order->get_user_id();

    if( $order ) {

        // Trigger new purchase
        do_action( 'gamipress_wc_new_purchase', $order_id, $user_id );

        $items = $order->get_items();

        if ( is_array( $items ) ) {

            // On purchase, trigger events on each product purchased
            foreach ( $items as $item ) {

                $product_id     = 0;
                $variation_id   = 0;
                $quantity       = 1;

                if( class_exists( 'WC_Order_Item' ) && $item instanceof WC_Order_Item ) {

                    // WooCommerce >= 3.0.0
                    $product_id     = $item->get_product_id();
                    $variation_id   = $item->get_variation_id();
                    $quantity       = $item->get_quantity();

                } else if( is_array( $item ) && isset( $item['product_id'] ) ) {

                    // WooCommerce < 3.0.0
                    $product_id     = $item['product_id'];
                    $variation_id   = ( isset( $item['variation_id'] ) ? $item['variation_id'] : 0 );
                    $quantity       = isset( $item['qty'] ) ? absint( $item['qty'] ) : 1;

                }

                if( $product_id !== 0 ) {

                    // Trigger events same times as item quantity
                    for ( $i = 0; $i < $quantity; $i++ ) {

                        $vendor_id = absint( get_post_field( 'post_author', $product_id ) );

                        if( $vendor_id !== 0 ) {
                            // Trigger new product sale to award the vendor
                            do_action( 'gamipress_wc_new_sale', $product_id, $vendor_id, $order_id );
                        }

                        // Trigger new product purchase
                        do_action( 'gamipress_wc_new_product_purchase', $product_id, $user_id, $order_id );

                        // Trigger specific product purchase
                        do_action( 'gamipress_wc_specific_product_purchase', $product_id, $user_id, $order_id );

                        if( $variation_id !== 0 ) {

                            // Trigger specific product variation purchase
                            do_action( 'gamipress_wc_product_variation_purchase', $product_id, $user_id, $variation_id, $order_id );

                        }

                        // Get an array of categories IDs attached to the product
                        $categories = wc_get_product_term_ids( $product_id, 'product_cat' );

                        if( ! empty( $categories ) ) {

                            foreach( $categories as $category_id ) {

                                // Trigger specific product category purchase (trigger 1 event per category)
                                do_action( 'gamipress_wc_product_category_purchase', $product_id, $user_id, $category_id, $order_id );
                            }

                        }

                        // Get an array of tags IDs attached to the product
                        $tags = wc_get_product_term_ids( $product_id, 'product_tag' );

                        if( ! empty( $tags ) ) {

                            foreach( $tags as $tag_id ) {
                                // Trigger specific product tag purchase (trigger 1 event per tag)
                                do_action( 'gamipress_wc_product_tag_purchase', $product_id, $user_id, $tag_id, $order_id );
                            }

                        }

                    }

                }

            } // end foreach

        } // end if $items is an array

    } // end if $order

}
add_action( 'woocommerce_payment_complete', 'gamipress_wc_new_purchase' );

// Refund purchase listener
function gamipress_wc_purchase_refund( $order_id ) {

    $order = wc_get_order( $order_id );
    $user_id = $order->get_user_id();

    if( $order ) {
        // Trigger purchase refund
        do_action( 'gamipress_wc_purchase_refund', $order_id, $user_id );

        $items = $order->get_items();

        if ( is_array( $items ) ) {

            // On purchase, trigger events on each product purchased
            foreach ( $items as $item ) {

                $product_id     = 0;
                $variation_id   = 0;
                $quantity       = 1;

                if( class_exists( 'WC_Order_Item' ) && $item instanceof WC_Order_Item ) {

                    // WooCommerce >= 3.0.0
                    $product_id     = $item->get_product_id();
                    $variation_id   = $item->get_variation_id();
                    $quantity       = $item->get_quantity();

                } else if( is_array( $item ) && isset( $item['product_id'] ) ) {

                    // WooCommerce < 3.0.0
                    $product_id     = $item['product_id'];
                    $variation_id   = ( isset( $item['variation_id'] ) ? $item['variation_id'] : 0 );
                    $quantity       = isset( $item['qty'] ) ? absint( $item['qty'] ) : 1;

                }

                if( $product_id !== 0 ) {

                    // Trigger events same times as item quantity
                    for ( $i = 0; $i < $quantity; $i++ ) {

                        $vendor_id = absint( get_post_field( 'post_author', $product_id ) );

                        if( $vendor_id !== 0 ) {
                            // Trigger product refunded to award the vendor
                            do_action( 'gamipress_wc_user_product_refund', $product_id, $vendor_id, $order_id );
                        }

                        // Trigger product refund
                        do_action( 'gamipress_wc_product_refund', $product_id, $user_id, $order_id );

                        // Trigger specific product refund
                        do_action( 'gamipress_wc_specific_product_refund', $product_id, $user_id, $order_id );

                        if( $variation_id !== 0 ) {

                            // Trigger specific product variation refund
                            do_action( 'gamipress_wc_product_variation_refund', $product_id, $user_id, $variation_id, $order_id );

                        }

                        // Get an array of categories IDs attached to the product
                        $categories = wc_get_product_term_ids( $product_id, 'product_cat' );

                        if( ! empty( $categories ) ) {

                            foreach( $categories as $category_id ) {

                                // Trigger specific product category refund (trigger 1 event per category)
                                do_action( 'gamipress_wc_product_category_refund', $product_id, $user_id, $category_id, $order_id );
                            }

                        }

                        // Get an array of tags IDs attached to the product
                        $tags = wc_get_product_term_ids( $product_id, 'product_tag' );

                        if( ! empty( $tags ) ) {

                            foreach( $tags as $tag_id ) {
                                // Trigger specific product tag refund (trigger 1 event per tag)
                                do_action( 'gamipress_wc_product_tag_refund', $product_id, $user_id, $tag_id, $order_id );
                            }

                        }

                    }

                }

            } // end foreach

        } // end if $items is an array

    } // end if $order

}

// Check order status changes to meet if should award to the user
function gamipress_wc_check_order_status_change( $order_id, $from, $to, $order ) {

    if( $from !== 'completed' && $to === 'completed' ) {
        gamipress_wc_new_purchase( $order_id );
    }

    if( $from !== 'refunded' && $to === 'refunded' ) {
        gamipress_wc_purchase_refund( $order_id );
    }

}
add_action( 'woocommerce_order_status_changed', 'gamipress_wc_check_order_status_change', 10, 4 );

// Review listener
function gamipress_wc_approved_review_listener( $comment_ID, $comment ) {

    // Enforce array for both hooks (wp_insert_comment uses object, comment_{status}_comment uses array)
    if ( is_object( $comment ) ) {
        $comment = get_object_vars( $comment );
    }

    // Check if comment is a review
    // In some release, WooCommerce stop to set the comment_type as review and now reviews are based on assigned post ID post_type
    //if ( $comment[ 'comment_type' ] !== 'review' ) {
    if ( get_post_type( $comment[ 'comment_post_ID' ] ) !== 'product' ) {
        return;
    }

    // Check if review is approved
    if ( 1 !== (int) $comment[ 'comment_approved' ] ) {
        return;
    }

    // Trigger review actions
    do_action( 'gamipress_wc_specific_new_review', (int) $comment_ID, (int) $comment[ 'user_id' ], $comment[ 'comment_post_ID' ], $comment );
    do_action( 'gamipress_wc_new_review', (int) $comment_ID, (int) $comment[ 'user_id' ], $comment[ 'comment_post_ID' ], $comment );

}
add_action( 'comment_approved_', 'gamipress_wc_approved_review_listener', 10, 2 );
add_action( 'comment_approved_review', 'gamipress_wc_approved_review_listener', 10, 2 );
add_action( 'wp_insert_comment', 'gamipress_wc_approved_review_listener', 10, 2 );