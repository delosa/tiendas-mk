<?php
/**
 * Ajax Functions
 *
 * @package GamiPress\WooCommerce\Ajax_Functions
 * @since 1.1.3
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

// Return product variations dropdown for requirements UI
function gamipress_wc_ajax_get_product_variations() {

    $product_id = $_POST['post_id'];
    $selected = $_POST['selected'];

    $product = wc_get_product( $product_id );

    // Bail if product doesn't exists
    if( ! $product ) {
        die();
    }

    $output = gamipress_wc_get_product_variations_dropdown( $product_id, $selected );

    // Bail if product has no variations
    if( empty( $output ) ) {
        echo '<span style="color: #a00;">This product has no variations</span>';
        die();
    }

    echo $output;
    die();

}
add_action( 'wp_ajax_gamipress_wc_get_product_variations', 'gamipress_wc_ajax_get_product_variations' );