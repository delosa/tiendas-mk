<?php
/**
 * Requirements
 *
 * @package GamiPress\WooCommerce\Requirements
 * @since 1.1.3
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

/**
 * Add custom fields to the requirement object
 *
 * @param $requirement
 * @param $requirement_id
 *
 * @return array
 */
function gamipress_wc_requirement_object( $requirement, $requirement_id ) {

    if( isset( $requirement['trigger_type'] )
        && ( $requirement['trigger_type'] === 'gamipress_wc_product_variation_purchase'
            || $requirement['trigger_type'] === 'gamipress_wc_product_variation_refund' ) ) {
        // Product variation
        $requirement['wc_variation_id'] = gamipress_get_post_meta( $requirement_id, '_gamipress_wc_variation_id', true );
    }

    if( isset( $requirement['trigger_type'] )
        && ( $requirement['trigger_type'] === 'gamipress_wc_product_category_purchase'
            || $requirement['trigger_type'] === 'gamipress_wc_product_category_refund' ) ) {
        // Category
        $requirement['wc_category_id'] = gamipress_get_post_meta( $requirement_id, '_gamipress_wc_category_id', true );
    }

    if( isset( $requirement['trigger_type'] )
        && ( $requirement['trigger_type'] === 'gamipress_wc_product_tag_purchase'
            || $requirement['trigger_type'] === 'gamipress_wc_product_tag_refund' ) ) {
        // Category
        $requirement['wc_tag_id'] = gamipress_get_post_meta( $requirement_id, '_gamipress_wc_tag_id', true );
    }

    return $requirement;
}
add_filter( 'gamipress_requirement_object', 'gamipress_wc_requirement_object', 10, 2 );

/**
 * Custom fields on requirements UI
 *
 * @param $requirement_id
 * @param $post_id
 */
function gamipress_wc_requirement_ui_fields( $requirement_id, $post_id ) {

    // Product variation select
    $post_id = absint( gamipress_get_post_meta( $requirement_id, '_gamipress_achievement_post' ) );
    $variation_id = absint( gamipress_get_post_meta( $requirement_id, '_gamipress_wc_variation_id', true ) );
    ?>

    <span class="wc-variation">
        <?php if( $post_id !== 0 && $variation_id !== 0 ) {
            echo gamipress_wc_get_product_variations_dropdown( $post_id, $variation_id );
        } ?>
    </span>

    <?php

    // Category select
    $category_id = absint( gamipress_get_post_meta( $requirement_id, '_gamipress_wc_category_id', true ) );
    $categories = get_terms( array(
        'taxonomy' => 'product_cat',
        'hide_empty' => false,
    ) );

    ?>

    <span class="wc-category">
        <select>
            <?php foreach( $categories as $category ) : ?>
                <option value="<?php echo $category->term_id; ?>" <?php selected( $category_id, $category->term_id ); ?>><?php echo $category->name; ?></option>
            <?php endforeach; ?>
        </select>
    </span>

    <?php

    // Tags select
    $tag_id = absint( gamipress_get_post_meta( $requirement_id, '_gamipress_wc_tag_id', true ) );
    $tags = get_terms( array(
        'taxonomy' => 'product_tag',
        'hide_empty' => false,
    ) );

    ?>

    <span class="wc-tag">
        <select>
            <?php foreach( $tags as $tag ) : ?>
                <option value="<?php echo $tag->term_id; ?>" <?php selected( $tag_id, $tag->term_id ); ?>><?php echo $tag->name; ?></option>
            <?php endforeach; ?>
        </select>
    </span>

    <?php
}
add_action( 'gamipress_requirement_ui_html_after_achievement_post', 'gamipress_wc_requirement_ui_fields', 10, 2 );

/**
 * Custom handler to save custom fields on requirements UI
 *
 * @param $requirement_id
 * @param $requirement
 */
function gamipress_wc_ajax_update_requirement( $requirement_id, $requirement ) {

    if( isset( $requirement['trigger_type'] )
        && ( $requirement['trigger_type'] === 'gamipress_wc_product_variation_purchase'
            || $requirement['trigger_type'] === 'gamipress_wc_product_variation_refund' ) ) {
        // Save the variation field
        gamipress_update_post_meta( $requirement_id, '_gamipress_wc_variation_id', $requirement['wc_variation_id'] );
    }

    if( isset( $requirement['trigger_type'] )
        && ( $requirement['trigger_type'] === 'gamipress_wc_product_category_purchase'
            || $requirement['trigger_type'] === 'gamipress_wc_product_category_refund' ) ) {
        // Save the category field
        gamipress_update_post_meta( $requirement_id, '_gamipress_wc_category_id', $requirement['wc_category_id'] );
    }

    if( isset( $requirement['trigger_type'] )
        && ( $requirement['trigger_type'] === 'gamipress_wc_product_tag_purchase'
            || $requirement['trigger_type'] === 'gamipress_wc_product_tag_refund' ) ) {
        // Save the tag field
        gamipress_update_post_meta( $requirement_id, '_gamipress_wc_tag_id', $requirement['wc_tag_id'] );
    }

}
add_action( 'gamipress_ajax_update_requirement', 'gamipress_wc_ajax_update_requirement', 10, 2 );